from lab4_triangulation.triangulation import get_triangulation_of_monotonic_with_scenes
from lab4_triangulation.triangulation_visualisation import LinesCollection
from lab4_triangulation.triangulation_visualisation import PointsCollection
from lab4_triangulation.triangulation_visualisation import Scene
from lab4_triangulation.triangulation_visualisation import Plot
import json as js

scenes = [Scene()]
plot1 = Plot(scenes)
plot1.draw()

with open('plot_data.json') as json_file:
    json_data = js.load(json_file)
    segments = json_data[0]["lines"]
    segments = segments[0]
    # for index, segment in enumerate(segments):
    #     segments[index] = segment[0]
    # if is_y_monotonic(segments):
    #     print("Monotonic")
    # else:
    #     print("Not monotonic")
    #
    # vertices_with_types = get_vertices_with_types(segments)
    # for vertex in vertices_with_types:
    #     print(vertex)
    # colored_vertices = get_PointsCollections_with_colored_vertices(vertices_with_types)
    # triangulated_segments = get_triangulation_of_monotonic(segments)
    # scenes = [Scene([], [LinesCollection(triangulated_segments)])]
    scenes = get_triangulation_of_monotonic_with_scenes(segments)
    plot1 = Plot(scenes)
    plot1.draw()
