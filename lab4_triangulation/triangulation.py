from enum import Enum

from lab4_triangulation.auxiliary_algorithms import det1
from lab4_triangulation.auxiliary_algorithms import angle_between_vectors
from lab4_triangulation.triangulation_visualisation import LinesCollection
from lab4_triangulation.triangulation_visualisation import PointsCollection
from lab4_triangulation.triangulation_visualisation import Scene


class VertexType(Enum):
    BEGINNING = 1
    ENDING = 2
    JOINING = 3
    DIVIDING = 4
    NORMAL = 5


def is_y_monotonic(segments: list):
    """alternatywnie można iśc po kolei i sprawdzać kolejne y-greki
    jeśli klejny y-grek mniejszy od poprzedniego, to znaczy że zawracamy
    jeśli teraz kolejny y-grek większy od poprzedniego to zwaróciliśmy ponownie -> ergo nie monotoniczny"""
    segment_copy = segments.copy()
    pois = []  # points of interest
    for i, segment in enumerate(segment_copy):
        pois.append((segment[0][0], segment[0][1],
                     i))  # save the points of the segment as points of interest along with index of the segment the belong to
        pois.append((segment[1][0], segment[1][1], i))
    pois.sort(key=lambda tup: tup[1], reverse=True)  # sort by y; top to bottom

    sweeper_state = []  # indices of segments that the sweeper intersects

    for poi in pois:
        poi_line_index = poi[2]
        if poi_line_index in sweeper_state:
            sweeper_state.remove(poi_line_index)
        else:
            sweeper_state.append(poi_line_index)
            if len(sweeper_state) >= 4:
                return False
    return True


def order_segments(segments: list):
    """:param segments: list of segments
    :return: ordered segments: each segment is connected with next and previous, last is connected to first
    connected == share a point
    unnecessary if data comes from _Button_callback.save_data"""
    if len(segments) < 2:
        return segments
    last_added_index = 0
    last_added_segment = segments[last_added_index]
    ordered_segments = [last_added_segment]
    finished = False
    while not finished:
        for index, segment in enumerate(segments):
            if index != last_added_index and segment not in ordered_segments:  # probably can be optimized
                if segment[0] in last_added_segment or segment[1] in last_added_segment:  # -> "if they share a point"
                    # TODO: make sure there's no situation like a -> b <- c (two segments end in b)
                    ordered_segments.append(segment)
                    last_added_segment = segment
                    last_added_index = index
                if len(ordered_segments) == len(segments):
                    finished = True
    # make sure they are counter-clockwise
    segment_with_lowest_point = min(ordered_segments, key=lambda segment: min(segment[0][1], segment[1][1]))
    segment_with_lowest_point_index = ordered_segments.index(segment_with_lowest_point)
    if segment_with_lowest_point[0][1] <= segment_with_lowest_point[1][1]:  # beginning of the segment is the lowest
        prev_p = ordered_segments[segment_with_lowest_point_index - 1][1]
        next_p = segment_with_lowest_point[1]
    else:  # ending of the segment is the lowest
        prev_p = segment_with_lowest_point[0]
        next_p = ordered_segments[segment_with_lowest_point_index + 1 % len(ordered_segments)][1]
    if prev_p[0] < next_p[0]:  # prev is to the left of the next; order is counterclockwise
        return ordered_segments
    else:
        return __reverse_segments(ordered_segments)


def __get_type_of_vertex(previous_p, p, next_p):
    angle = angle_between_vectors(next_p, p,
                                  previous_p)  # order of arguments is reverted because of how angle is calculated
    if previous_p[1] > p[1] and next_p[1] > p[1]:  # (simply test showed this order works ;) )
        if angle < 180:
            return VertexType.ENDING
        else:
            return VertexType.JOINING
    elif previous_p[1] < p[1] and next_p[1] < p[1]:
        if angle < 180:
            return VertexType.BEGINNING
        else:
            return VertexType.DIVIDING
    else:
        return VertexType.NORMAL


def is_counter_clockwise(segments: list):
    """znaleźć najwyższy, sprwawdzić, czy poprzednik jest po prawej od następnika, zwrócić prawdę jeśli tak, fałsz wpp"""
    pass


def get_vertices_with_types(segments: list):
    ordered_segments = order_segments(segments)
    vertices = []  # with type
    for index, segment in enumerate(ordered_segments):
        prev_p = ordered_segments[index - 1][0]
        p = segment[0]
        next_p = segment[1]
        vertices.append((p, __get_type_of_vertex(prev_p, p, next_p)))
    return vertices


def __reverse_segments(segments: list):
    segments_copy = segments.copy()
    segments_copy.reverse()
    for segment in segments:
        segment[0], segment[1] = segment[1], segment[0]  # swap ending of the segment with it's beginning
    return segments_copy


def is_triangle_inside_figure(starting_v, middle_v, end_v, left_vertices, right_vertices):
    if end_v in right_vertices:
        return det1(starting_v, middle_v, end_v) < 0
    elif end_v in left_vertices:
        return det1(starting_v, middle_v, end_v) > 0
    else:
        print("ERROR end_v seems not to belong to neither list of left nor right vertices")
        return None
        # error wtf


def get_triangulation_of_monotonic(segments: list):
    if not is_y_monotonic(segments):
        print("The figure is not monotonic. Requirement is not met. Aborting.")
        return None
    ordered_segments = order_segments(segments)
    ordered_vertices = []
    for segment in ordered_segments:
        ordered_vertices.append(segment[0])  # vertices ordered clockwise
    highest_vertex = max(ordered_vertices, key=lambda tup: tup[1])
    lowest_vertex = min(ordered_vertices, key=lambda tup: tup[1])
    highest_vertex_index = ordered_vertices.index(highest_vertex)
    lowest_vertex_index = ordered_vertices.index(lowest_vertex)
    left = [highest_vertex]
    right = [lowest_vertex]
    add_to_left = True
    i = (highest_vertex_index + 1) % len(ordered_vertices)
    while len(left) + len(right) != len(ordered_vertices):
        if add_to_left:
            if i != lowest_vertex_index:
                left.append(ordered_vertices[i])
                i += 1
                i = i % len(ordered_vertices)
            else:
                add_to_left = False
                i += 1
                i = i % len(ordered_vertices)
        else:
            right.append(ordered_vertices[i])
            i += 1
            i = i % len(ordered_vertices)

    segments_copy = segments.copy()
    # left is already top to bottom
    right.reverse()
    vertices_top_to_bottom = ordered_vertices.copy()
    vertices_top_to_bottom.sort(key=lambda tup: tup[1], reverse=True)
    stack = [vertices_top_to_bottom[0], vertices_top_to_bottom[1]]  # these should be two top vertices
    for i in range(2, len(vertices_top_to_bottom)):
        if (vertices_top_to_bottom[i] in left and stack[-1] not in left) or (
                vertices_top_to_bottom[i] in right and stack[-1] not in right):
            # next vertex is on the other side than the one from stack
            for vertex in stack:
                segments_copy.append([vertex, vertices_top_to_bottom[i]])
            stack = [vertices_top_to_bottom[i - 1], vertices_top_to_bottom[i]]
        else:
            added_new_triangle = False
            middle_v = stack.pop()
            tmp_stack = []  # temporary structure to keep processed elements away so that the weren't processed twice
            while len(stack) > 0 and not added_new_triangle:
                s = stack.pop()
                if is_triangle_inside_figure(s, middle_v, vertices_top_to_bottom[i], left, right):
                    segments_copy.append([s, vertices_top_to_bottom[i]])
                    tmp_stack.append(vertices_top_to_bottom[i])
                    tmp_stack.append(s)
                    added_new_triangle = True
                else:
                    tmp_stack.append(s)
            tmp_stack.reverse()  # to keep previous order
            stack.extend(tmp_stack)
            if not added_new_triangle:
                stack.append(middle_v)
                stack.append(vertices_top_to_bottom[i])
            tmp_stack.clear()

    return segments_copy


def get_triangulation_of_monotonic_with_scenes(segments: list):
    scenes = []
    if not is_y_monotonic(segments):
        print("The figure is not monotonic. Requirement is not met. Aborting.")
        return None
    ordered_segments = order_segments(segments)
    ordered_vertices = []
    for segment in ordered_segments:
        ordered_vertices.append(segment[0])  # vertices ordered clockwise
    highest_vertex = max(ordered_vertices, key=lambda tup: tup[1])
    lowest_vertex = min(ordered_vertices, key=lambda tup: tup[1])
    highest_vertex_index = ordered_vertices.index(highest_vertex)
    lowest_vertex_index = ordered_vertices.index(lowest_vertex)
    left = [highest_vertex]
    right = [lowest_vertex]
    add_to_left = True
    i = (highest_vertex_index + 1) % len(ordered_vertices)
    while len(left) + len(right) != len(ordered_vertices):
        if add_to_left:
            if i != lowest_vertex_index:
                left.append(ordered_vertices[i])
                i += 1
                i = i % len(ordered_vertices)
            else:
                add_to_left = False
                i += 1
                i = i % len(ordered_vertices)
        else:
            right.append(ordered_vertices[i])
            i += 1
            i = i % len(ordered_vertices)

    segments_copy = segments.copy()
    # left is already top to bottom
    right.reverse()
    vertices_top_to_bottom = ordered_vertices.copy()
    vertices_top_to_bottom.sort(key=lambda tup: tup[1], reverse=True)
    stack = [vertices_top_to_bottom[0], vertices_top_to_bottom[1]]  # these should be two top vertices
    scenes.append(Scene([PointsCollection(stack.copy(), color='red')], [LinesCollection(segments_copy)]))
    for i in range(2, len(vertices_top_to_bottom)):
        if (vertices_top_to_bottom[i] in left and stack[-1] not in left) or (
                vertices_top_to_bottom[i] in right and stack[-1] not in right):
            # next vertex is on the other side than the one from stack
            for vertex in stack:
                new_line = [vertex, vertices_top_to_bottom[i]]
                scenes.append(Scene([PointsCollection(stack.copy(), color='red'),
                                     PointsCollection([vertices_top_to_bottom[i]], color='lime')],
                                    [LinesCollection(segments_copy.copy()), LinesCollection([new_line], color="pink")]))
                segments_copy.append(new_line)
            stack = [vertices_top_to_bottom[i - 1], vertices_top_to_bottom[i]]
            scenes.append(Scene([PointsCollection(stack.copy(), color='red')],
                                [LinesCollection(segments_copy.copy())]))
        else:  # next vertex is on the same side as the one from stack
            added_new_triangle = False
            middle_v = stack.pop()
            tmp_stack = []  # temporary structure to keep processed elements away so that the weren't processed twice
            while len(stack) > 0 and not added_new_triangle:
                s = stack.pop()
                new_line = [s, vertices_top_to_bottom[i]]

                red_vertices = stack.copy() + tmp_stack.copy() + [s, vertices_top_to_bottom[i]]
                scenes.append(Scene([PointsCollection(red_vertices, color='red'),
                                     PointsCollection([middle_v], color='gold')],
                                    [LinesCollection(segments_copy.copy()),
                                     LinesCollection([new_line], color="pink")]))
                if is_triangle_inside_figure(s, middle_v, vertices_top_to_bottom[i], left, right):
                    segments_copy.append(new_line)
                    tmp_stack.append(vertices_top_to_bottom[i])
                    tmp_stack.append(s)
                    added_new_triangle = True
                    scenes.append(Scene([PointsCollection(stack.copy() + tmp_stack.copy(), color='red'),
                                         PointsCollection([middle_v], color='gold')],
                                        [LinesCollection(segments_copy.copy())]))
                else:
                    tmp_stack.append(s)
                    scenes.append(Scene([PointsCollection(stack.copy() + tmp_stack.copy(), color='red'),
                                         PointsCollection([middle_v], color='gold')],
                                        [LinesCollection(segments_copy.copy())]))
            tmp_stack.reverse()  # to keep previous order
            stack.extend(tmp_stack)
            if not added_new_triangle:
                stack.append(middle_v)
                stack.append(vertices_top_to_bottom[i])
            tmp_stack.clear()
            scenes.append(Scene([PointsCollection(stack.copy(), color='red')],
                                [LinesCollection(segments_copy.copy())]))
    return scenes
