na zajęciach ukończyłem wprowadzanie i sprawdzanie monotoniczności
sprawdzanie y-monotoniczności
	użyłem algorytmu zamiatania z poprzedniego laboratorium z tym, że zamiast sprawdzać przecięcia, sprawdzam liczebność odcinków w stanie miotły
		ze względu na sposób podania figury jako listy odcinków, jeśli len(stan_miotły) >=4 to figura nie jest monotoniczna
		podczas laboratoriów prosiła Pani, żeby sprawdzić algorytm dla figur mających coś na kształ zębów piły oraz dla kwadratu- działa dla wszystkich
kolorowanie wierzchołków według typu
	kolory przyjąłem mniej więcej takie, jakie były na prezentacji z moodla
	przetestowałem ponownie dla "zębów" i trójkątów w różnych orientacjach, wielokąta (prawie) foremnego, kwadratu i kilku bliżej nieokreślonych figur; wygląda na to, że działa wyśmienicie
	
triangulacja
	wielokąt przechowywany jest jako lista odcinków, przy czym koniec jednego to początek kolejnego
		dodatkowo z tej listy wydzieliłem sobie wierzchołki, żeby wygodniej było je dzielić i obsługiwać
		wierzchołki są sortowane od góry do dołu
		podział na lewą i prawą wygląda tak, że najwyższy wierzchołek jest zawsze w grupie prawych, zaś najniższy w lewych; pozostałe oczywiście też są w odpowiedniej grupie
	triangulacja jest przechowywana jako po prostu lista odcinków, bez żadnych dodatkowych warunków; dzięki temu zrobienie scen było bardzo proste
	
	przetestowałem dla: 
		trójkąta kwadratu, żeby sprawdzić szczególne przypadki
		"zębów", według zaleceń z zajęć
		wielokąta przypominającego grot strzały i "w miarę foremnego" wielokąta, żeby sprawdzić przypadki ogólne
	wygląda na to, że działa

	EDYCJA PO KONSULTACJI NA OSTATNICH ZAJĘCIACH:
	    znacząco poprawiłem algorytm, między innymi użyłem wyznacznika to sprawdzania przynależności trójkąta do figury
	    tak jak Pani zaleciła
	    załączyłem garść przykładów w formie zdjęć, żeby nadorbić niemożność osobistej prezentacji