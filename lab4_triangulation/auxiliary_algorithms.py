import math

default_precision = 0.00000000000001    # 1 e -14

def det1(a, b, c):
    ax = a[0]
    ay = a[1]
    bx = b[0]
    by = b[1]
    cx = c[0]
    cy = c[1]
    return ax * by + ay * cx + bx * cy - cx * by - ax * cy - ay * bx


def is_on_line_segment(s1p1, s1p2, p):
    """Assuming the points are collinear"""
    s1p1_x = s1p1[0]
    s1p2_x = s1p2[0]
    p_x = p[0]
    s1p1_y = s1p1[1]
    s1p2_y = s1p2[1]
    p_y = p[1]
    if (min(s1p1_x, s1p2_x) < p_x < max(s1p1_x, s1p2_x)) and \
            (min(s1p1_y, s1p2_y) < p_y < max(s1p1_y, s1p2_y)):
        return True
    return False


def do_segments_intersect(segment1, segment2):
    s1p1 = segment1[0]
    s1p2 = segment1[1]
    s2p1 = segment2[0]
    s2p2 = segment2[1]
    d1 = det1(s2p1, s2p2, s1p1)
    d2 = det1(s2p1, s2p2, s1p2)
    d3 = det1(s1p1, s1p2, s2p1)
    d4 = det1(s1p1, s1p2, s2p2)
    if d1 * d2 < 0 and d3 * d4 < 0:
        return True
    if (math.fabs(d1) < default_precision and is_on_line_segment(s2p1, s2p2, s1p1)) or \
            (math.fabs(d2) < default_precision and is_on_line_segment(s2p1, s2p2, s1p2)) or \
            (math.fabs(d3) < default_precision and is_on_line_segment(s1p1, s1p2, s2p1)) or \
            (math.fabs(d4) < default_precision and is_on_line_segment(s1p1, s1p2, s2p2)):
        return True
    return False


def angle_between_vectors(a, b, c):
    """returns angle between b->c and X axis minus angle between b->c and X axis
    uses math.atan2 which returns atan(y / x), in radians. The result is between -pi and pi. The vector in the plane from the
    origin to point (x, y) makes this angle with the positive X axis. """
    ang = math.degrees(math.atan2(c[1] - b[1], c[0] - b[0]) - math.atan2(a[1] - b[1], a[0] - b[0]))
    return ang + 360 if ang < 0 else ang