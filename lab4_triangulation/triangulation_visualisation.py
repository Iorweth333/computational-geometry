import json as js
from enum import Enum

import matplotlib.collections as mcoll
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Button

TOLERANCE = 0.15


class VertexType(Enum):
    BEGINNING = 1
    ENDING = 2
    JOINING = 3
    DIVIDING = 4
    NORMAL = 5

    def __eq__(self, other: object) -> bool:
        return self.value == other.value


def dist(point1, point2):
    return np.sqrt(np.power(point1[0] - point2[0], 2) + np.power(point1[1] - point2[1], 2))


class _Button_callback(object):
    def __init__(self, scenes):
        self.i = 0
        self.scenes = scenes
        self.adding_points = False
        self.added_points = []
        self.adding_lines = False
        self.added_lines = []
        self.adding_rects = False
        self.added_rects = []

    def set_axes(self, ax):
        self.ax = ax

    def next(self, event):
        self.i = (self.i + 1) % len(self.scenes)
        self.draw(autoscaling=True)

    def prev(self, event):
        self.i = (self.i - 1) % len(self.scenes)
        self.draw(autoscaling=True)

    def add_point(self, event):
        self.adding_points = not self.adding_points
        self.new_line_point = None
        if self.adding_points:
            self.adding_lines = False
            self.adding_rects = False
            self.added_points.append(PointsCollection([]))

    def add_line(self, event):
        self.adding_lines = not self.adding_lines
        self.new_line_point = None
        if self.adding_lines:
            self.adding_points = False
            self.adding_rects = False
            self.added_lines.append(LinesCollection([]))

    def add_rect(self, event):
        self.adding_rects = not self.adding_rects
        self.new_line_point = None
        if self.adding_rects:
            self.adding_points = False
            self.adding_lines = False
            self.new_rect()

    def new_rect(self):
        self.added_rects.append(LinesCollection([]))
        self.rect_points = []

    def save_data(self, event):
        points = self.added_points
        lines = self.added_lines
        rects = self.added_rects
        scene = Scene(points, lines + rects)
        plot = Plot([scene])
        data = plot.toJson()
        file = open("plot_data.json", 'w+')
        file.write(data)
        print("Saved data to file " + file.name)

    def on_click(self, event):
        if event.inaxes != self.ax:
            return
        new_point = (event.xdata, event.ydata)
        if self.adding_points:
            self.added_points[-1].add_points([new_point])
            self.draw(autoscaling=False)
        elif self.adding_lines:
            if self.new_line_point is not None:
                self.added_lines[-1].add([self.new_line_point, new_point])
                self.new_line_point = None
                self.draw(autoscaling=False)
            else:
                self.new_line_point = new_point
        elif self.adding_rects:
            if len(self.rect_points) == 0:
                self.rect_points.append(new_point)
            elif len(self.rect_points) == 1:
                self.added_rects[-1].add([self.rect_points[-1], new_point])
                self.rect_points.append(new_point)
                self.draw(autoscaling=False)
            elif len(self.rect_points) > 1:
                if dist(self.rect_points[0], new_point) < (
                        np.mean([self.ax.get_xlim(), self.ax.get_ylim()]) * TOLERANCE):
                    self.added_rects[-1].add([self.rect_points[-1], self.rect_points[0]])
                    self.new_rect()
                else:
                    self.added_rects[-1].add([self.rect_points[-1], new_point])
                    self.rect_points.append(new_point)
                self.draw(autoscaling=False)

    def draw(self, autoscaling=True):
        if not autoscaling:
            xlim = self.ax.get_xlim()
            ylim = self.ax.get_ylim()
        self.ax.clear()
        for collection in (self.scenes[self.i].points + self.added_points):
            if len(collection.points) > 0:
                self.ax.scatter(*zip(*(np.array(collection.points))), **collection.kwargs)
        for collection in (self.scenes[self.i].lines + self.added_lines + self.added_rects):
            self.ax.add_collection(collection.get_collection())
        self.ax.autoscale(autoscaling)
        if not autoscaling:
            self.ax.set_xlim(xlim)
            self.ax.set_ylim(ylim)
        plt.draw()


class Scene:
    def __init__(self, points=[], lines=[]):
        self.points = points
        self.lines = lines


class PointsCollection:
    def __init__(self, points, **kwargs):
        self.points = points
        self.kwargs = kwargs

    def add_points(self, points):
        self.points = self.points + points


class LinesCollection:
    def __init__(self, lines, **kwargs):
        self.lines = lines
        self.kwargs = kwargs

    def add(self, line):
        self.lines.append(line)

    def get_collection(self):
        return mcoll.LineCollection(self.lines, **self.kwargs)


class Plot:
    def __init__(self, scenes=[Scene()], json=None):
        if json is None:
            self.scenes = scenes
        else:
            self.scenes = [Scene([PointsCollection(pointsCol) for pointsCol in scene["points"]],
                                 [LinesCollection(linesCol) for linesCol in scene["lines"]])
                           for scene in js.loads(json)]

    def __configure_buttons(self):
        plt.subplots_adjust(bottom=0.2)
        ax_add_line = plt.axes([0.18, 0.05, 0.15, 0.075])
        ax_add_point = plt.axes([0.34, 0.05, 0.15, 0.075])
        ax_prev = plt.axes([0.50, 0.05, 0.15, 0.075])
        ax_next = plt.axes([0.66, 0.05, 0.15, 0.075])
        ax_save_data = plt.axes([0.82, 0.05, 0.15, 0.075])
        ax_add_rect = plt.axes([0.02, 0.05, 0.15, 0.075])
        b_next = Button(ax_next, 'Następny')
        b_next.on_clicked(self.callback.next)
        b_prev = Button(ax_prev, 'Poprzedni')
        b_prev.on_clicked(self.callback.prev)
        b_add_point = Button(ax_add_point, 'Dodaj punkt')
        b_add_point.on_clicked(self.callback.add_point)
        b_add_line = Button(ax_add_line, 'Dodaj linię')
        b_add_line.on_clicked(self.callback.add_line)
        b_add_rect = Button(ax_add_rect, 'Dodaj figurę')
        b_add_rect.on_clicked(self.callback.add_rect)
        b_save_data = Button(ax_save_data, 'Zapisz dane')
        b_save_data.on_clicked(self.callback.save_data)
        return [b_prev, b_next, b_add_point, b_add_line, b_add_rect, b_save_data]

    def add_scene(self, scene):
        self.scenes.append(scene)

    def add_scenes(self, scenes):
        self.scenes = self.scenes + scenes

    def toJson(self):
        return js.dumps([{"points": [np.array(pointCol.points).tolist() for pointCol in scene.points],
                          "lines": [linesCol.lines for linesCol in scene.lines]}
                         for scene in self.scenes])

    def get_added_points(self):
        if self.callback:
            return self.callback.added_points
        else:
            return None

    def get_added_lines(self):
        if self.callback:
            return self.callback.added_lines
        else:
            return None

    def get_added_figure(self):
        if self.callback:
            return self.callback.added_rects
        else:
            return None

    def get_added_elements(self):
        if self.callback:
            return Scene(self.callback.added_points, self.callback.added_lines + self.callback.added_rects)
        else:
            return None

    def draw(self):
        plt.close()
        fig = plt.figure()
        self.callback = _Button_callback(self.scenes)
        self.widgets = self.__configure_buttons()
        ax = plt.axes(autoscale_on=False)
        self.callback.set_axes(ax)
        fig.canvas.mpl_connect('button_press_event', self.callback.on_click)
        plt.show()
        self.callback.draw()


def get_PointsCollections_with_colored_vertices(vertices_with_types: list):
    """map is used to get rid of the vertices types because they are no longer useful in collections"""
    beginning = list(map(lambda vwt: vwt[0], filter(lambda p: p[1] == VertexType.BEGINNING, vertices_with_types)))
    beginning_collection = PointsCollection(beginning, color='green')
    ending = list(map(lambda vwt: vwt[0], filter(lambda p: p[1] == VertexType.ENDING, vertices_with_types)))
    ending_collection = PointsCollection(ending, color='red')
    dividing = list(map(lambda vwt: vwt[0], filter(lambda p: p[1] == VertexType.DIVIDING, vertices_with_types)))
    dividing_collection = PointsCollection(dividing, color='aqua')
    joining = list(map(lambda vwt: vwt[0], filter(lambda p: p[1] == VertexType.JOINING, vertices_with_types)))
    joining_collection = PointsCollection(joining)  # default, blue
    normal = list(map(lambda vwt: vwt[0], filter(lambda p: p[1] == VertexType.NORMAL, vertices_with_types)))
    normal_collection = PointsCollection(normal, color='brown')
    return [beginning_collection, ending_collection, dividing_collection, joining_collection, normal_collection]


# przykład użycia

scenes = [Scene([PointsCollection([(1, 2), (3, 1.5), (2, -1)]),
                 PointsCollection([(5, -2), (2, 2), (-2, -1)], color='green', marker="^")],
                [LinesCollection([[(1, 2), (2, 3)], [(0, 1), (1, 0)]])]),
          Scene([PointsCollection([(1, 2), (3, 1.5), (2, -1)], color='red'),
                 PointsCollection([(5, -2), (2, 2), (-2, 1)], color='black')],
                [LinesCollection([[(-1, 2), (-2, 3)], [(0, -1), (-1, 0)]])])]
# koniec przykładu
