from lab2_convex_aureola.graham_algorithm import graham
from lab2_convex_aureola.jarvis_algorithm import jarvis
from lab2_convex_aureola import random_data
from lab2_convex_aureola.interactive_visualisation_classes import Scene
from lab2_convex_aureola.interactive_visualisation_classes import PointsCollection
from lab2_convex_aureola.interactive_visualisation_classes import LinesCollection
from lab2_convex_aureola.interactive_visualisation_classes import Plot
import time


def divide_into_subgroups(list: list, chunks_size: int):  # chunks_size = m
    chunks = []
    for i in range(0, len(list), chunks_size):
        chunks.append(list[i:i + chunks_size])
    return chunks


def convex_hull_chan(points: list, group_size: int):
    subgroups = divide_into_subgroups(points, group_size)
    number_of_subgroups = len(subgroups)
    subhulls = []
    for subgroup in subgroups:
        if len(subgroup) > 2:
            subhulls.extend(graham(subgroup))
        else:
            subhulls.extend(subgroup)
    return jarvis(subhulls)


def convex_hull_chan_with_scenes(points: list, group_size: int):
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    if len(points) / group_size > len(colors) - 1:
        print("Too many groups to color them all. Aborting")
        return None
    colors_iterator1 = 1
    colors_iterator2 = 1
    scenes = [Scene([PointsCollection(points)])]
    subgroups = divide_into_subgroups(points, group_size)
    subgroups_points_collections = []
    subgroups_hulls_collections = [PointsCollection(points)]
    subhulls = []
    for subgroup in subgroups:
        subgroups_points_collections.append(PointsCollection(subgroup, color=colors[colors_iterator1]))
        colors_iterator1 += 1
        if len(subgroup) > 2:
            subhull = graham(subgroup)
            subhulls.extend(subhull)
            subgroups_hulls_collections.append(PointsCollection(subhull, color=colors[colors_iterator2]))
            colors_iterator2 += 1
        else:
            subhulls.extend(subgroup)
            subgroups_hulls_collections.append(PointsCollection(subgroup, color=colors[colors_iterator2]))
            colors_iterator2 += 1
    final_hull = jarvis(subhulls)
    scenes.append(Scene(subgroups_points_collections))  # divided into gropus
    scenes.append(
        Scene(subgroups_hulls_collections))  # with found hulls of subgropus; points not beloging to any hull are blue
    scenes.append(Scene([PointsCollection(points), PointsCollection(final_hull, color='red')]))  # final scene
    return scenes


# random_data.set_custom_seed(123456789)
#
# # points = random_data.rectangle(points_number=30)  #nie używać kwadratowych, bo jarvis sie na nich psuje
# points = random_data.in_square(60, -1000, 1000)
# # # hull = convex_hull_chan(points, 1000)
# # time_start = time.process_time()
# # hull = convex_hull_chan(points,5) #5,71
# # hull = graham(points)   #6,71
# # time_stop = time.process_time()
# # print(time_stop - time_start)
# #
# # scenes = [Scene([PointsCollection(points),
# #                  PointsCollection(hull, 'red')])]
# scenes = convex_hull_chan_with_scenes(points, 10)
# #
# if scenes is not None:
#     plot = Plot(scenes)
#     plot.draw()
#
# times_mapping = []
# file = open("time_data4.txt", 'w+', buffering=1)
# # time_function = time.process_time
# # time_function = time.clock
# time_function = time.perf_counter
#
# for group_size in range(5500, 5501, 500):
#     time_start = time_function()
#     hull = convex_hull_chan(points, group_size)
#     time_stop = time_function()
#     work_time = time_stop - time_start
#     # times_mapping.append((group_size, work_time))
#     file.write(str((group_size, work_time)))
#     file.write("\n")
#     # if group_size % 1000 == 0:
#     #     file.flush()
#     #     print("Flushed")
# # print(times_mapping)
#
# # for mapping in times_mapping:

