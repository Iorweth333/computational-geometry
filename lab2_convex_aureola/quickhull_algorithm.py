from lab2_convex_aureola.interactive_visualisation_classes import Scene
from lab2_convex_aureola.interactive_visualisation_classes import PointsCollection

qucikhull = []


def findSide(p1, p2, p):
    val = (p[1] - p1[1]) * (p2[0] - p1[0]) - (p2[1] - p1[1]) * (p[0] - p1[0])
    if val > 0:
        return 1
    if val < 0:
        return -1
    return 0


def lineDist(p1, p2, p):
    return abs((p[1] - p1[1]) * (p2[0] - p1[0]) - (p2[1] - p1[1]) * (p[0] - p1[0]))


def quickHull(a, n, p1, p2, side):
    ind = -1
    max_dist = 0

    for i in range(0, n):
        temp = lineDist(p1, p2, a[i])
        if (findSide(p1, p2, a[i]) == side and temp > max_dist):
            ind = i
            max_dist = temp

    if (ind == -1):
        qucikhull.append(p1)
        qucikhull.append(p2)
        return

    quickHull(a, n, a[ind], p1, -findSide(a[ind], p1, p2))
    quickHull(a, n, a[ind], p2, -findSide(a[ind], p2, p1))


def getQuickHull(a):
    n = len(a)
    if (n < 3):
        print("Convex hull not possible\n")
        return

    min_x = 0
    max_x = 0
    for i in range(0, n):
        if (a[i][0] < a[min_x][0]):
            min_x = i
        if (a[i][0] > a[max_x][0]):
            max_x = i

    quickHull(a, n, a[min_x], a[max_x], 1)
    quickHull(a, n, a[min_x], a[max_x], -1)

    return qucikhull


quickhull = []
tmp_points = []
scenes = []


def quickHull_with_scenes(a, n, p1, p2, side):
    global quickhull
    global tmp_points
    ind = -1
    max_dist = 0
    if p1 not in tmp_points:    #p1 i p2 to punkty, według których następuje podział
        tmp_points.append(p1)
    if p2 not in tmp_points:
        tmp_points.append(p2)

    for i in range(0, n):
        temp = lineDist(p1, p2, a[i])
        if findSide(p1, p2, a[i]) == side and temp > max_dist:
            ind = i
            max_dist = temp

    if ind == -1:
        quickhull.append(p1)
        quickhull.append(p2)
        scenes.append(Scene(
            [PointsCollection(a), PointsCollection(tmp_points, color='k'), PointsCollection(quickhull, color='r')]))
        return
    scenes.append(
        Scene([PointsCollection(a), PointsCollection(tmp_points, color='k'), PointsCollection([p1, p2], color='y'),
               PointsCollection([a[ind]], color='m')]))
    quickHull_with_scenes(a, n, a[ind], p1, -findSide(a[ind], p1, p2))
    quickHull_with_scenes(a, n, a[ind], p2, -findSide(a[ind], p2, p1))


def getQuickHull_with_scenes(a):
    global quickhull
    global scenes
    global tmp_points
    quickhull = []
    tmp_points = []
    scenes = [Scene([PointsCollection(a)])]
    n = len(a)
    if (n < 3):
        print("Convex hull not possible\n")
        return

    min_x = 0
    max_x = 0
    for i in range(0, n):
        if (a[i][0] < a[min_x][0]):
            min_x = i
        if (a[i][0] > a[max_x][0]):
            max_x = i

    scenes.append(Scene([PointsCollection(a), PointsCollection([a[min_x], a[max_x]], color='y')]))

    quickHull_with_scenes(a, n, a[min_x], a[max_x], 1)
    quickHull_with_scenes(a, n, a[min_x], a[max_x], -1)

    return scenes

# getQuickHull(points)
