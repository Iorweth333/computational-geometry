from lab2_convex_aureola.interactive_visualisation_classes import Scene
from lab2_convex_aureola.interactive_visualisation_classes import PointsCollection
from lab2_convex_aureola.interactive_visualisation_classes import LinesCollection
from lab2_convex_aureola.interactive_visualisation_classes import Plot


def orientation_dc(p, q, r):
    val = (q[1] - p[1]) * (r[0] - q[0]) - (r[1] - q[1]) * (q[0] - p[0])
    # print(val)

    if val == 0:
        return 0
    elif val > 0:
        return 1
    else:
        return -1


def merge(left_half, right_half):
    n1 = len(left_half)
    n2 = len(right_half)

    p = 0
    q = 0

    for i in range(1, n1):
        if (left_half[i][0] > left_half[ia][0]):
            p = i

    for i in range(1, n2):
        if (right_half[i][0] > right_half[ib][0]):
            q = i

    indp = p
    indq = q
    done = 0

    while not (done):
        done = 1
        while (orientation_dc(right_half[indq], left_half[indp], left_half[(indp + 1) % n1]) >= 0):
            indp = (indp + 1) % n1

        while (orientation_dc(left_half[indp], right_half[indq], right_half[(n2 + indq - 1) % n2]) <= 0):
            indb = (n2 + indb - 1) % n2
            done = 0

    upperp = indp
    upperq = indq
    indp = ia
    indq = ib

    done = 0
    g = 0
    while not (done):  # finding the lower tangent
        done = 1;
        while (orientation_dc(left_half[indp], right_half[indq], right_half[(indq + 1) % n2]) >= 0):
            indq = (indq + 1) % n2

        while (orientation_dc(right_half[indq], left_half[indp], left_half[(n1 + indp - 1) % n1]) <= 0):
            indp = (n1 + indp - 1) % n1
            done = 0

    lowerp = indp
    lowerq = indq
    ret = []

    ind = uppera
    ret.append(left_half[uppera])
    while (ind != lowera):
        ind = (ind + 1) % n1
        ret.append(left_half[ind])

    ind = lowerb
    ret.append(right_half[lowerb])
    while (ind != upperb):
        ind = (ind + 1) % n2
        ret.append(right_half[ind])

    return ret


def merge_with_scenes(left_half, righ_half):
    n1 = len(left_half)
    n2 = len(right_half)

    p = 0
    q = 0

    for i in range(1, n1):
        if (left_half[i][0] > left_half[ia][0]):
            p = i

    for i in range(1, n2):
        if (right_half[i][0] > right_half[ib][0]):
            q = i

    indp = p
    indq = q
    done = 0

    while not (done):
        done = 1
        while (orientation_dc(right_half[indq], left_half[indp], left_half[(indp + 1) % n1]) >= 0):
            indp = (indp + 1) % n1

        while (orientation_dc(left_half[indp], right_half[indq], right_half[(n2 + indq - 1) % n2]) <= 0):
            indb = (n2 + indb - 1) % n2
            done = 0

    upperp = indp
    upperq = indq
    indp = ia
    indq = ib

    done = 0
    g = 0
    while not (done):  # finding the lower tangent
        done = 1;
        while (orientation_dc(left_half[indp], right_half[indq], right_half[(indq + 1) % n2]) >= 0):
            indq = (indq + 1) % n2

        while (orientation_dc(right_half[indq], left_half[indp], left_half[(n1 + indp - 1) % n1]) <= 0):
            indp = (n1 + indp - 1) % n1
            done = 0

    lowerp = indp
    lowerq = indq
    ret = []

    ind = uppera

    ret.append(left_half[uppera])

    scenes = [Scene([PointsCollection(left_half, 'y'), PointsCollection(righ_half, 'm'), PointsCollection(ret, 'r')])]

    while (ind != lowera):
        ind = (ind + 1) % n1
        ret.append(left_half[ind])
        scenes = [
            Scene([PointsCollection(left_half, 'y'), PointsCollection(righ_half, 'm'), PointsCollection(ret, 'r')])]

    ind = lowerb
    ret.append(right_half[lowerb])
    scenes = [Scene([PointsCollection(left_half, color='y'), PointsCollection(righ_half, color='m'),
                     PointsCollection(ret, color='r')])]
    while (ind != upperb):
        ind = (ind + 1) % n2
        ret.append(right_half[ind])
        scenes = [Scene([PointsCollection(left_half, color='y'), PointsCollection(righ_half, color='m'),
                         PointsCollection(ret, color='r')])]
    return scenes


def bruteHull(points):
    s = []

    for i in range(0, len(points)):
        for j in range(i + 1, len(points)):
            x1 = points[i][0]
            x2 = points[j][0]
            y1 = points[i][1]
            y2 = points[j][1]

            a1 = y1 - y2
            b1 = x2 - x1
            c1 = x1 * y2 - y1 * x2
            pos = 0
            neg = 0
            for k in range(0, len(points)):
                if (a1 * points[k][0] + b1 * points[k][1] + c1 <= 0):
                    neg += 1
                if (a1 * points[k][0] + b1 * points[k][1] + c1 >= 0):
                    pos += 1

            if (pos == len(points) or neg == len(points)):
                s.append(points[i])
                s.append(points[j])

    ret = []
    for e in s:
        ret.append(e)

    # Sorting the points in the anti-clockwise order
    mid = (0, 0)
    sum0 = 0
    sum1 = 0

    n = len(ret)
    for i in range(0, n):
        sum0 += ret[i][0]
        sum1 += ret[i][1]
        ret1n = ret[i][1] * n
        ret0n = ret[i][1] * n
        ret[i] = (ret0n, ret1n)

    mid = (sum0, sum1)

    ret = sorted(ret, key=lambda p: p[0])
    for i in range(0, n):
        ret[i] = ((ret[i][0] / n, ret[i][1] / n))

    return ret


def convex_hull_dc(points):
    if (len(points) <= 5):

        return bruteHull(points)

    left_points = []
    right_points = []

    for i in range(0, int(len(points) / 2)):
        left_points.append(points[i])
    for i in range(int(len(points) / 2), len(points)):
        right_points.append(points[i])

    left_half = convex_hull_dc(left_points)
    right_half = convex_hull_dc(right_points)
    return merge(left_half, right_half)


def convex_hull_dc_with_scenes(points):
    scenes = [Scene([PointsCollection(points)])]
    if (len(points) <= 5):
        scenes.append(Scene([PointsCollection(points), PointsCollection(bruteHull(points), color='r')]))
        return scenes
    left_points = []
    right_points = []

    for i in range(0, int(len(points) / 2)):
        left_points.append(points[i])
    for i in range(int(len(points) / 2), len(points)):
        right_points.append(points[i])

    scenes.append(Scene([PointsCollection(left_points, color='y'), PointsCollection(right_points, color='m')]))

    left_half = convex_hull_dc(left_points)
    right_half = convex_hull_dc(right_points)

    scenes.append(Scene(
        [PointsCollection(points), PointsCollection(left_half, color='y'), PointsCollection(right_half, color='m')]))

    return scenes.extend(merge_with_scenes(left_half, right_half))
