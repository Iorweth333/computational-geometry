from lab2_convex_aureola.auxiliary_algorithms import angle_with_x_axis_for_vector
from lab2_convex_aureola.auxiliary_algorithms import angle_between_vectors
from lab2_convex_aureola.interactive_visualisation_classes import PointsCollection
from lab2_convex_aureola.interactive_visualisation_classes import Scene

scenes = []
aureola_points = []


def find_lowest_point_index(points: list):
    min_y_index = 0
    min_y = points[0][1]

    for i in range(1, len(points)):
        if points[i][1] < min_y:
            min_y_index = i
            min_y = points[i][1]

    return min_y_index


def find_minimal_angle(points: list, edge_beginning_point, edge_end_point=None, add_scene=False):
    global scenes
    global aureola_points
    indices_angles = []
    if edge_end_point is None:
        for index, point in enumerate(points):
            if point[0] != edge_beginning_point[0] or point[1] != edge_beginning_point[1]:
                angle = angle_with_x_axis_for_vector(edge_beginning_point, point)
                indices_angles.append((index, angle))
    else:
        # start_end_angle = angle_with_x_axis_for_vector(edge_beginning_point, edge_end_point)
        for index, point in enumerate(points):
            if (point[0] != edge_beginning_point[0] or point[1] != edge_beginning_point[1]) and \
                    (point[0] != edge_end_point[0] or point[1] != edge_end_point[1]):
                # other than edge beginning and other than edge end
                # angle = angle_with_x_axis_for_vector(edge_end_point, point) - start_end_angle
                angle = angle_between_vectors(edge_beginning_point, edge_end_point, point)
                indices_angles.append((index, angle))
                if add_scene:
                    scenes.append(Scene([PointsCollection(points), PointsCollection(aureola_points, color='red'),
                                         PointsCollection([edge_beginning_point, edge_end_point], color='black'),
                                         PointsCollection([point], color='yellow')]))

    result = min(indices_angles, key=lambda tup: tup[1])
    return result[0]  # return just index


def jarvis(points: list):
    starting_point_index = find_lowest_point_index(points)
    starting_point = points[starting_point_index]
    second_point_index = find_minimal_angle(points, starting_point)
    second_point = points[second_point_index]
    next_point_index = find_minimal_angle(points, starting_point, second_point)
    aureola_points = [starting_point, points[second_point_index]]
    while next_point_index != starting_point_index:
        aureola_points.append(points[next_point_index])
        next_point_index = find_minimal_angle(points, aureola_points[-2], aureola_points[-1])
    return aureola_points


def jarvis_with_scenes(points: list):
    global scenes
    scenes = [Scene([PointsCollection(points)])]  # clear
    starting_point_index = find_lowest_point_index(points)
    starting_point = points[starting_point_index]
    scenes.append(Scene([PointsCollection(points), PointsCollection([starting_point], color='red')]))
    second_point_index = find_minimal_angle(points, starting_point, add_scene=True)
    second_point = points[second_point_index]
    scenes.append(Scene([PointsCollection(points), PointsCollection([starting_point, second_point], color='red')]))
    next_point_index = find_minimal_angle(points, starting_point, second_point, add_scene=False)
    global aureola_points
    aureola_points = [starting_point, points[second_point_index]]
    while next_point_index != starting_point_index:
        aureola_points.append(points[next_point_index])
        scenes.append(Scene([PointsCollection(points), PointsCollection(aureola_points, color='red')]))
        next_point_index = find_minimal_angle(points, aureola_points[-2], aureola_points[-1], add_scene=True)
    scenes.append(Scene([PointsCollection(points), PointsCollection(aureola_points, color='red')]))
    return scenes

# Scene([PointsCollection([(1, 2), (3, 1.5), (2, -1)]),
#                PointsCollection([(5, -2), (2, 2), (-2, -1)], 'green')],
#               [LinesCollection([[(1,2),(2,3)], [(0,1),(1,0)]])]),
