import math
from lab1_determinants.determinants import det1
from lab2_convex_aureola.auxiliary_algorithms import angle_with_x_axis_for_vector
from lab2_convex_aureola.interactive_visualisation_classes import PointsCollection
from lab2_convex_aureola.interactive_visualisation_classes import Scene

# precision = 0.000000000000001
scenes = []
aureola = []

precision = 0.0000000000001
def pick_point_with_minimum_y(points):
    min_y_point = points[0]
    min_y = points[0][1]
    min_x_among_min_y = points[0][0]
    for point in points:
        x = point[0]
        y = point[1]
        if y < min_y:
            min_y_point = point
            min_y = y
            min_x_among_min_y = x
        elif y == min_y:
            if x < min_x_among_min_y:
                min_y_point = point
                min_y = y
                min_x_among_min_y = x
    return min_y_point


def two_points_distance(point1, point2):
    x1 = point1[0]
    x2 = point2[0]
    y1 = point1[1]
    y2 = point2[1]
    return math.hypot(x2 - x1, y2 - y1)  # scaling to origin point


def __sort_by_angle_and_remove_irrelevant(points_with_data: list):
    points_with_data.sort(key=lambda tup: tup[2])  # sort by angle, sorts in place
    indices_to_delete = set()
    for i in range(len(points_with_data) - 1):  # - 1 because i dint need to check the last
        j = 1
        number_of_same_angle_points_in_a_row = 1
        while i + j < len(points_with_data) and points_with_data[i][2] == points_with_data[i + j][2]:
            j += 1
            number_of_same_angle_points_in_a_row += 1

        if number_of_same_angle_points_in_a_row > 1:
            max_dist_so_far = points_with_data[i][3]
            max_dist_so_far_index = i
            for k in range(i + 1, i + number_of_same_angle_points_in_a_row):    # range does not include the last
                this_dist = points_with_data[k][3]
                if max_dist_so_far >= this_dist:
                    indices_to_delete.add(k)
                else:
                    max_dist_so_far = this_dist
                    indices_to_delete.add(max_dist_so_far_index)
                    max_dist_so_far_index = k

    list_of_indices_to_delete = list(indices_to_delete)
    list_of_indices_to_delete.sort(reverse=True)
    for index in list_of_indices_to_delete:
        del points_with_data[index]
    return points_with_data


def sort_by_angle_with_x_axis(points, p0):
    the_other_points = points.copy()
    the_other_points.remove(p0)
    the_other_points_with_data = []
    for point in the_other_points:
        the_other_points_with_data.append(
            (point[0], point[1], angle_with_x_axis_for_vector(p0, point), two_points_distance(p0, point)))

    the_other_points_with_data = __sort_by_angle_and_remove_irrelevant(the_other_points_with_data)
    result = []
    for index, point in enumerate(the_other_points_with_data):  #cut unnecessary data
        result.append([point[0], point[1]])
    return result


def graham(points: list):
    p0 = pick_point_with_minimum_y(points)
    p = sort_by_angle_with_x_axis(points, p0)
    # p = [(tup[0], tup[1]) for tup in p]  # cut off unnecessary data
    stack = [p0, p[0], p[1]]
    i = 2                       # because 0-based indexing
    m = len(p)
    while i < m:
        if det1(stack[-2], stack[-1], p[i]) > precision:
            stack.append(p[i])
            i += 1
        else:
            stack.pop()
    return stack

def graham_with_scenes(points: list):
    global scenes
    scenes = [Scene([PointsCollection(points)])]  # clear
    p0 = pick_point_with_minimum_y(points)
    p = sort_by_angle_with_x_axis(points, p0)
    # p = [(tup[0], tup[1]) for tup in p]  # cut off unnecessary data
    stack = [p0, p[0], p[1]]
    i = 2  # because 0-based indexing
    m = len(p)
    while i < m:
        edge_beginning_point = stack[-2]
        edge_end_point = stack[-1]
        point = p[i]
        scenes.append(Scene([PointsCollection(points), PointsCollection(stack, color='red'),
                             PointsCollection([list(edge_beginning_point), list(edge_end_point)], color='black'),
                             PointsCollection([list(point)], color='yellow')]))

        if det1(edge_beginning_point, edge_end_point, p[i]) > precision:
            stack.append(p[i])
            i += 1
        else:
            stack.pop()
    scenes.append(Scene([PointsCollection(points), PointsCollection(stack, color='red')]))
    return scenes
