import math


def angle_with_x_axis_for_vector(point1, point2):  # point1 will be p0
    x1 = point1[0]
    x2 = point2[0]
    y1 = point1[1]
    y2 = point2[1]
    if y1 <= y2:
        return math.atan2(y2 - y1, x2 - x1)  # scaled to origin point
    else:
        return math.atan2(y2 - y1, x2 - x1) + 2 * math.pi  # plus pi for i want a positive angle

def angle_between_vectors(a, b, c):
    """return angle between vector a->b and b->c
    technically it's angle between b->c and X axis minus angle between b->c and X axis
    uses math.atan2 which returns atan(y / x), in radians. The result is between -pi and pi. The vector in the plane from the
    origin to point (x, y) makes this angle with the positive X axis. """
    ang = math.degrees(math.atan2(c[1]-b[1], c[0]-b[0]) - math.atan2(a[1]-b[1], a[0]-b[0]))
    return ang + 360 if ang < 0 else ang