from lab2_convex_aureola import random_data
from lab2_convex_aureola import graham_algorithm
from lab2_convex_aureola import jarvis_algorithm
from lab2_convex_aureola.quickhull_algorithm import getQuickHull
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.collections as mcoll
import matplotlib.colors as mcolors
import json as js
import time


class PointsCollection:
    def __init__(self, points=[], color=None, marker=None):
        self.points = np.array(points)
        self.color = color
        self.marker = marker


class LinesCollection:
    def __init__(self, lines=[], color=None):
        self.color = color
        self.lines = lines

    def add(self, line):
        self.lines.append(line)

    def get_collection(self):
        if self.color:
            return mcoll.LineCollection(self.lines, [mcolors.to_rgba(self.color)] * len(lines))
        else:
            return mcoll.LineCollection(self.lines)


class Plot:
    def __init__(self, points=[], lines=[], json=None):
        if json is None:
            self.points = points
            self.lines = lines
        else:
            self.points = [PointsCollection(pointsCol) for pointsCol in js.loads(json)["points"]]
            self.lines = [LinesCollection(linesCol) for linesCol in js.loads(json)["lines"]]

    def draw(self):
        plt.close()
        ax = plt.axes()
        for collection in self.points:
            if collection.points.size > 0:
                ax.scatter(*zip(*collection.points), c=collection.color, marker=collection.marker)
        for collection in self.lines:
            ax.add_collection(collection.get_collection())
        ax.autoscale()
        plt.show()

    def toJSON(self):
        return js.dumps({"points": [pointCol.points.tolist() for pointCol in self.points],
                         "lines": [linesCol.lines for linesCol in self.lines]})


random_data.set_custom_seed(123456789)

original_data = PointsCollection([(-9.0155326273843, -10.0),
                                  (9.800418988962647, -10.0),
                                  (10.0, -8.84290853680223),
                                  (10.0, 9.967973934417074),
                                  (6.335581949980398, 10.0),
                                  (-9.47577927417857, 10.0),
                                  (-9.902878676788532, 10.0),
                                  (-10.0, 9.166010893994084),
                                  (-10.0, -9.55702728096518)])

original_data = PointsCollection(random_data.in_square())
# original_data = PointsCollection(random_data.on_circle())
# original_data = PointsCollection(random_data.rectangle())
# original_data = PointsCollection(random_data.on_square())

# GRAHAM

# aureola = PointsCollection(graham_algorithm.graham(original_data.points.tolist()), color='red')
# plot = Plot([original_data, aureola])
# plot.draw()

#

#
# original_data = PointsCollection(random_data.in_square())
# aureola = PointsCollection(graham_algorithm.graham(original_data.points.tolist()), color='red')
# plot = Plot([original_data, aureola])
# plot.draw()
#
# original_data = PointsCollection(random_data.on_circle())
# aureola = PointsCollection(graham_algorithm.graham(original_data.points.tolist()), color='red')
# plot = Plot([original_data, aureola])
# plot.draw()
#
# original_data = PointsCollection(random_data.rectangle())
# aureola = PointsCollection(graham_algorithm.graham(original_data.points.tolist()), color='red')
# plot = Plot([original_data, aureola])
# plot.draw()
#
# original_data = PointsCollection(random_data.on_square())
# aureola = PointsCollection(graham_algorithm.graham(original_data.points.tolist()), color='red')
# plot = Plot([original_data, aureola])
# plot.draw()

# JARVIS

# original_data = PointsCollection([(0, 0),
#                                   (1, 0),
#                                   (1, 1),
#                                   (0, 1),
#                                   (0.5, 0.5),
#                                   (0.5, 2)])

# original_data = PointsCollection([(0, 0),
#                                   (1, 0),
#                                   (1, 1),
#                                   (0.5,0),
#                                   (1,0.5),
#                                   (0,1)
#                                   ])

# original_data = PointsCollection([(0, 0),
#                                   (1, 0),
#                                   (0.5, 0),
#                                   (0, 1),
#                                   (1, 1),
#                                   (1, 0.5),
#                                   ])

points_list = original_data.points.tolist()

time_start = time.process_time()
# aureola = jarvis_algorithm.jarvis(points_list)
aureola = getQuickHull(points_list)
time_stop = time.process_time()
elapsed_time = time_stop - time_start
print(elapsed_time)

aureola_collection = PointsCollection(aureola, color='red')

plot = Plot([original_data, aureola_collection])
plot.draw()



