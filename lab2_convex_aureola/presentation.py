from lab2_convex_aureola.random_data import in_square
from lab2_convex_aureola.random_data import set_custom_seed
from lab2_convex_aureola.jarvis_algorithm import jarvis_with_scenes
from lab2_convex_aureola.graham_algorithm import graham_with_scenes
from lab2_convex_aureola.quickhull_algorithm import getQuickHull_with_scenes
from lab2_convex_aureola.interactive_visualisation_classes import PointsCollection
from lab2_convex_aureola.interactive_visualisation_classes import Plot
from lab2_convex_aureola.divide_and_conquer import convex_hull_dc_with_scenes
from lab2_convex_aureola.chan_algorithm import convex_hull_chan_with_scenes

set_custom_seed(123456789)
points = in_square(15, -20, 20)

# JARVIS
scenes = jarvis_with_scenes(points)
plot = Plot(scenes)
plot.draw("JARVIS")

# GRAHAM
scenes = graham_with_scenes(points)
plot = Plot(scenes)
plot.draw("GRAHAM")

# QUICKHULL
# zielonym oraz fioletowym kolorem zaznaczone są punkty tworzące trójkąt, wewnątrz którego punkty odrzucamy,
# czarnym są te, które były zielone lub fioletowe w innej gałęzi rekurencji,
# czerwonym te, które stanowią otoczkę
scenes = getQuickHull_with_scenes(points)
plot = Plot(scenes)
plot.draw("QUICKHULL")

#CHAN
# każdy kolor to jedna podgrupa
# ciemnoniebieskie punkty na trzeciej scenie nie należą do żadnej z otoczek podgrup
points = in_square(30, -20, 20)
scenes = convex_hull_chan_with_scenes(points, 10)
plot = Plot(scenes)
plot.draw("CHAN")










# DZIEL I RZĄDŹ
# scenes = convex_hull_dc_with_scenes(points)
# plot = Plot(scenes)
# plot.draw()
