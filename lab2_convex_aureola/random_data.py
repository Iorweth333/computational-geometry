import random
import math


def set_custom_seed(seed):
    random.seed(seed)


def in_square(points_number=100, min_coord=-100, max_coord=100):
    points = []
    for i in range(points_number):
        pointX = random.uniform(min_coord, max_coord)
        pointY = random.uniform(min_coord, max_coord)
        points.append((pointX, pointY))
    return points


def on_circle(center=(0, 0), points_number=100, radius=10):
    points = []
    center_x = center[0]
    center_y = center[1]
    for i in range(points_number):
        # a = random.uniform(0, radius)  #uncomment to make them appear INSIDE the circle
        t = random.uniform(0, 2 * math.pi)
        x = radius * math.cos(t) + center_x
        y = radius * math.sin(t) + center_y
        points.append((x, y))
    return points


def rectangle(points_number=100, top_left=(-10, 10), top_right=(10, 10), bot_left=(-10, -10), bot_right=(10, -10)):
    points = []
    for i in range(points_number):
        pick = random.randint(1, 4)
        if pick == 1:
            # left
            x = top_left[0]
            y = random.uniform(bot_left[1], top_left[1])

        elif pick == 2:
            # top
            y = top_left[1]
            x = random.uniform(top_left[0], top_right[0])
        elif pick == 3:
            # right
            x = top_right[0]  # 10
            y = random.uniform(bot_right[1], top_right[1])  # (-10, 10)
        else:
            # bottom
            y = bot_left[1]  # -10
            x = random.uniform(bot_left[0], bot_right[0])  # (-10, 10)
        points.append((x, y))
    return points


def on_square(top_left=(0, 10), top_right=(10, 10), bot_left=(0, 0), bot_right=(10, 0), points_on_axis_num=25,
              points_on_diagonals_num=20):
    points = [bot_left, bot_right, top_right, top_left]  # rogi (wierzchołki)
    for i in range(points_on_axis_num):
        # boki na osiach
        x = bot_left[0]  # 0
        y = random.uniform(bot_left[1], top_left[1])
        points.append((x, y))
        y = bot_left[1]  # 0
        x = random.uniform(bot_left[0], bot_right[0])
        points.append((x, y))

    for i in range(points_on_diagonals_num):
        # na przekątnych
        x = random.uniform(bot_left[0], top_right[0])
        points.append((x, x))
        x = random.uniform(top_left[0], bot_right[0])
        y = top_left[1] - x
        points.append((x, y))
    return points
