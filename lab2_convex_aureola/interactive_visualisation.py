from lab2_convex_aureola import graham_algorithm
from lab2_convex_aureola import jarvis_algorithm
from lab2_convex_aureola import quickhull_algorithm
from lab2_convex_aureola import random_data
from lab2_convex_aureola.interactive_visualisation_classes import PointsCollection
from lab2_convex_aureola.interactive_visualisation_classes import Plot

random_data.set_custom_seed(123456789)
original_data = PointsCollection(random_data.in_square())
# original_data = PointsCollection(random_data.on_circle())
# original_data = PointsCollection(random_data.rectangle())
# original_data = PointsCollection(random_data.on_square())

# JARVIS
# scenes = jarvis_algorithm.jarvis_with_scenes(original_data.points.tolist())
# plot = Plot(scenes)
# plot.draw()


# GRAHAM

# scenes = graham_algorithm.graham_with_scenes(original_data.points.tolist())
# plot = Plot(scenes)
# plot.draw()


# QUICKHULL
scenes = quickhull_algorithm.getQuickHull_with_scenes(original_data.points.tolist())
plot = Plot(scenes)
plot.draw()