import random
import math


def points_in_range_1000():
    points = []
    for i in range(100000):
        pointX = random.uniform(-1000, 1000)
        pointY = random.uniform(-1000, 1000)
        points.append((pointX, pointY))
    return points


def points_in_range_thousand_trilion():
    points = []
    for i in range(100000):
        pointX = random.uniform(-10 ** 14, 10 ** 14)
        pointY = random.uniform(-10 ** 14, 10 ** 14)
        points.append((pointX, pointY))
    return points


def points_on_circle(radius):
    points = []
    for i in range(1000):
        #a = random.uniform(0, radius)  #uncomment to make them appear INSIDE the circle
        a = radius
        t = random.uniform(0, 2 * math.pi)
        x = a * math.cos(t)
        y = a * math.sin(t)
        points.append((x, y))
    return points


def points_on_straight():  # y = 0.05x + 0.05
    points = []

    for i in range(1000):
        x = random.uniform(-1000, 1000)
        y = 0.05 * x + 0.05
        points.append((x, y))
    return points
