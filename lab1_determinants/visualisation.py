import numpy as np
import matplotlib.pyplot as plt
import matplotlib.collections as mcoll
import matplotlib.colors as mcolors
import json as js
from lab1_determinants import random_data
from lab1_determinants import determinants as dets
import sys

orig_stdout = sys.stdout
file = open('log.txt', 'w')
sys.stdout = file


class PointsCollection:
    def __init__(self, points=[], color=None, marker=None):
        self.points = np.array(points)
        self.color = color
        self.marker = marker


class LinesCollection:
    def __init__(self, lines=[], color=None):
        self.color = color
        self.lines = lines

    def add(self, line):
        self.lines.append(line)

    def get_collection(self):
        if self.color:
            return mcoll.LineCollection(self.lines, [mcolors.to_rgba(self.color)] * len(lines))
        else:
            return mcoll.LineCollection(self.lines)


class Plot:
    def __init__(self, points=[], lines=[], json=None):
        if json is None:
            self.points = points
            self.lines = lines
        else:
            self.points = [PointsCollection(pointsCol) for pointsCol in js.loads(json)["points"]]
            self.lines = [LinesCollection(linesCol) for linesCol in js.loads(json)["lines"]]

    def draw(self):
        plt.close()
        ax = plt.axes()
        for collection in self.points:
            if collection.points.size > 0:
                ax.scatter(*zip(*collection.points), c=collection.color, marker=collection.marker)
        for collection in self.lines:
            ax.add_collection(collection.get_collection())
        ax.autoscale()
        plt.show()

    def toJSON(self):
        return js.dumps({"points": [pointCol.points.tolist() for pointCol in self.points],
                         "lines": [linesCol.lines for linesCol in self.lines]})


def print_comparison(det1, det2, det1_np, det2_np):
    print("\t\tdet1\tdet2\tdet1_np\tdet2_np")
    print('\t\t{0}\t{1}\t{2}\t{3}'.format(len(det1), len(det2), len(det1_np), len(det2_np)))


# przykład użycia
# %matplotlib notebook  #to przed wywoławyaniem metody plot w jupyterze
#
# plot = Plot([PointsCollection([(1, 2), (3, 1.5), (2, -1)]),
#              PointsCollection([(5, -2), (2, 2), (-2, -1)], color='green', marker="^")],
#             [LinesCollection([[(-1, 2), (-2, 3)]])])
# plot.draw()

print("10^5 losowych punktów o współrzędnych z przedziału [-1000, 1000]")
small_square = [PointsCollection(random_data.points_in_range_1000())]
plot = Plot(points=small_square)
plot.draw()

print("10^5 losowych punktów o współrzędnych z przedziału [-10^14, 10^14]")
big_square = [PointsCollection(random_data.points_in_range_thousand_trilion())]
plot = Plot(points=big_square)
plot.draw()

print("1000 losowych punktów leżących na okręgu o środku (0,0) i promieniu R=100")
circle = [PointsCollection(random_data.points_on_circle(100))]
plot = Plot(points=circle)
plot.draw()

print("1000 losowych punktów o współrzędnych z przedziału [-1000,  1000] leżących na prostej wyznaczonej "
      "przez wektor (a, b), przyjmij a=[-1.0, 0.0], b=[1.0, 0.1].")  # jest to, nawiasem mówiąc, prosta y = 0.5x + 0.5
straight = [PointsCollection(random_data.points_on_straight())]
plot = Plot(points=straight)
plot.draw()

a = (-1, 0)
b = (1, 0.1)
print("dokonuje podzialu wedlug prostej: ")
print(a, b)


# small_square_left1 = []
# small_square_right1 = []
# small_square_equal1 = []
# big_square_left1 = []
# big_square_right1 = []
# big_square_equal1 = []
# circle_left1 = []
# circle_right1 = []
# circle_equal1 = []
# straight_left1 = []
# straight_right1 = []
# straight_equal1 = []
# # noinspection DuplicatedCode
# # small_square_left2 = []
# # small_square_right2 = []
# # small_square_equal2 = []
# # big_square_left2 = []
# # big_square_right2 = []
# # big_square_equal2 = []
# circle_left2 = []
# circle_right2 = []
# circle_equal2 = []
# straight_left2 = []
# straight_right2 = []
# straight_equal2 = []

def test_for_precision(precision=0.00001):
    print("precyzja: ", precision)
    small_square_left1, small_square_equal1, small_square_right1 = dets.divide_using_det(dets.det1, a, b,
                                                                                         small_square[0].points,
                                                                                         precision)
    small_square_left2, small_square_equal2, small_square_right2 = dets.divide_using_det(dets.det2, a, b,
                                                                                         small_square[0].points,
                                                                                         precision)

    big_square_left1, big_square_equal1, big_square_right1 = dets.divide_using_det(dets.det1, a, b,
                                                                                   big_square[0].points,
                                                                                   precision)
    big_square_left2, big_square_equal2, big_square_right2 = dets.divide_using_det(dets.det2, a, b,
                                                                                   big_square[0].points,
                                                                                   precision)

    circle_left1, circle_equal1, circle_right1 = dets.divide_using_det(dets.det1, a, b, circle[0].points, precision)
    circle_left2, circle_equal2, circle_right2 = dets.divide_using_det(dets.det2, a, b, circle[0].points, precision)

    straight_left1, straight_equal1, straight_right1 = dets.divide_using_det(dets.det1, a, b, straight[0].points,
                                                                             precision)
    straight_left2, straight_equal2, straight_right2 = dets.divide_using_det(dets.det2, a, b, straight[0].points,
                                                                             precision)

    # numpy

    small_square_left1_np, small_square_equal1_np, small_square_right1_np = dets.divide_using_det(dets.det1_np, a, b,
                                                                                                  small_square[
                                                                                                      0].points,
                                                                                                  precision)
    small_square_left2_np, small_square_equal2_np, small_square_right2_np = dets.divide_using_det(dets.det2_np, a, b,
                                                                                                  small_square[
                                                                                                      0].points,
                                                                                                  precision)

    big_square_left1_np, big_square_equal1_np, big_square_right1_np = dets.divide_using_det(dets.det1_np, a, b,
                                                                                            big_square[0].points,
                                                                                            precision)
    big_square_left2_np, big_square_equal2_np, big_square_right2_np = dets.divide_using_det(dets.det2_np, a, b,
                                                                                            big_square[0].points,
                                                                                            precision)

    circle_left1_np, circle_equal1_np, circle_right1_np = dets.divide_using_det(dets.det1_np, a, b, circle[0].points,
                                                                                precision)
    circle_left2_np, circle_equal2_np, circle_right2_np = dets.divide_using_det(dets.det2_np, a, b, circle[0].points,
                                                                                precision)

    straight_left1_np, straight_equal1_np, straight_right1_np = dets.divide_using_det(dets.det1_np, a, b,
                                                                                      straight[0].points, precision)
    straight_left2_np, straight_equal2_np, straight_right2_np = dets.divide_using_det(dets.det2_np, a, b,
                                                                                      straight[0].points, precision)

    print("Small square:")
    print("\tleft")
    print_comparison(small_square_left1, small_square_left2, small_square_left1_np, small_square_left2_np)
    print("\tright")
    print_comparison(small_square_right1, small_square_right2, small_square_right1_np, small_square_right2_np)
    print("\tequal")
    print_comparison(small_square_equal1, small_square_equal2, small_square_equal1_np, small_square_equal2_np)

    print("Big square:")
    print("\tleft")
    print_comparison(big_square_left1, big_square_left2, big_square_left1_np, big_square_left2_np)
    print("\tright")
    print_comparison(big_square_right1, big_square_right2, big_square_right1_np, big_square_right2_np)
    print("\tequal")
    print_comparison(big_square_equal1, big_square_equal2, big_square_equal1_np, big_square_equal2_np)

    print("Circle:")
    print("\tleft")
    print_comparison(circle_left1, circle_left2, circle_left1_np, circle_left2_np)
    print("\tright")
    print_comparison(circle_right1, circle_right2, circle_right1_np, circle_right2_np)
    print("\tequal")
    print_comparison(circle_equal1, circle_equal2, circle_equal1_np, circle_equal2_np)

    print("Straight")
    print("\tleft")
    print_comparison(straight_left1, straight_left2, straight_left1_np, straight_left2_np)
    print("\tright")
    print_comparison(straight_right1, straight_right2, straight_right1_np, straight_right2_np)
    print("\tequal")
    print_comparison(straight_equal1, straight_equal2, straight_equal1_np, straight_equal2_np)


test_for_precision(0.1)
test_for_precision(0.01)
test_for_precision(0.001)
test_for_precision(0.0001)
test_for_precision(0.00001)
test_for_precision(0.000001)
test_for_precision(0.0000001)
test_for_precision(0.00000001)
test_for_precision(0.000000001)
test_for_precision(0.0000000001)
test_for_precision(0.00000000001)
test_for_precision(0.000000000001)
test_for_precision(0.0000000000001)
test_for_precision(0.00000000000001)
print("finish")
