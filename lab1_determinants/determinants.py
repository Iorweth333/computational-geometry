# < 0 --> po prawej
# > 0 --> po lewej
import numpy as np
from numpy import linalg
import math

default_precision = 0.00000000000001    # 1 e -14

def det1(a, b, c):
    ax = a[0]
    ay = a[1]
    bx = b[0]
    by = b[1]
    cx = c[0]
    cy = c[1]
    return ax * by + ay * cx + bx * cy - cx * by - ax * cy - ay * bx


def det1_np(a, b, c):
    ax = a[0]
    ay = a[1]
    bx = b[0]
    by = b[1]
    cx = c[0]
    cy = c[1]
    arr = np.array([[ax, ay, 1], [bx, by, 1], [cx, cy, 1]])
    return linalg.det(arr)


def det2(a, b, c):
    ax = a[0]
    ay = a[1]
    bx = b[0]
    by = b[1]
    cx = c[0]
    cy = c[1]
    acx = ax - cx
    acy = ay - cy
    bcx = bx - cx
    bcy = by - cy
    return acx * bcy - acy * bcx


def det2_np(a, b, c):
    ax = a[0]
    ay = a[1]
    bx = b[0]
    by = b[1]
    cx = c[0]
    cy = c[1]
    acx = ax - cx
    acy = ay - cy
    bcx = bx - cx
    bcy = by - cy
    arr = np.array([[acx, acy], [bcx, bcy]])
    return linalg.det(arr)


# def divide_using_det1(a, b, points, precision=0.00001):
#     left = []
#     equal = []
#     right = []
#     for point in points:
#         if det1(a, b, point) <= precision:
#             equal.append(point)
#         elif det1(a, b, point) > precision:
#             left.append(point)
#         else:
#             right.append(point)
#     return left, equal, right
#
#
# def divide_using_det2(a, b, points, precision=0.00001):
#     left = []
#     equal = []
#     right = []
#     for point in points:
#         if det2(a, b, point) <= precision:
#             equal.append(point)
#         elif det2(a, b, point) > precision:
#             left.append(point)
#         else:
#             right.append(point)
#     return left, equal, right


def divide_using_det(det_fun, *args):
    left = []
    equal = []
    right = []
    a = args[0]
    b = args[1]
    points = args[2]
    precision = args[3]
    for point in points:
        if math.fabs(det_fun(a, b, point)) <= precision:
            equal.append(point)
        elif det_fun(a, b, point) > precision:
            left.append(point)
        else:
            right.append(point)
    return left, equal, right
