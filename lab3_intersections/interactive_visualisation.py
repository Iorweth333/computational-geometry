import numpy as np
import matplotlib.pyplot as plt
import matplotlib.collections as mcoll
import matplotlib.colors as mcolors
from matplotlib.widgets import Button
import json as js
import lab3_intersections.random_data


class _Button_callback(object):
    def __init__(self, scenes):
        self.i = 0
        self.scenes = scenes
        self.adding_points = False
        self.added_points = []
        self.adding_lines = False
        self.added_lines = []

    def set_axes(self, ax):
        self.ax = ax

    def next(self, event):
        # print("_Button_callback; next; event:")
        # print(event)
        self.i = (self.i + 1) % len(self.scenes)
        self.draw()

    def prev(self, event):
        # print("_Button_callback; prev; event:")
        # print(event)
        self.i = (self.i - 1) % len(self.scenes)
        self.draw()

    def draw(self):
        self.ax.clear()
        for collection in (self.scenes[self.i].points + self.added_points):
            if len(collection.points) > 0:
                self.ax.scatter(*zip(*(np.array(collection.points))), **collection.kwargs)
        for collection in (self.scenes[self.i].lines + self.added_lines):
            self.ax.add_collection(collection.get_collection())
        self.ax.autoscale()
        plt.draw()

    def add_point(self, event):
        # print("_Button_callback; add_point; event:")
        # print(event)
        self.adding_points = not self.adding_points
        self.new_line_point = None
        if self.adding_points:
            self.adding_lines = False
            self.added_points.append(PointsCollection())

    def add_line(self, event):
        # print("_Button_callback; add_line; event:")
        # print(event)
        self.adding_lines = not self.adding_lines
        self.new_line_point = None
        if self.adding_lines:
            self.adding_points = False
            self.added_lines.append(LinesCollection())

    def add_random_lines(self, event):
        # print("_Button_callback; add_random_lines; event:")
        # print(event)
        lines_collection = lab3_intersections.random_data.random_lines()
        self.added_lines.append(lines_collection)
        self.draw()

    def save_data(self, event):
        points = self.added_points
        lines =  self.added_lines
        scene = Scene(points, lines)
        plot = Plot([scene])
        data = plot.toJson()
        file = open("plot_data.json", 'w+')
        file.write(data)
        print("Saved data to file " + file.name)

    def on_click(self, event):
        # print("_Button_callback; on_click; event:")
        # print(event)
        if event.inaxes != self.ax:
            return
        new_point = (event.xdata, event.ydata)
        if self.adding_points:
            self.added_points[-1].add_points([new_point])
            self.draw()
        elif self.adding_lines:
            if self.new_line_point is not None:
                self.added_lines[-1].add([self.new_line_point, new_point])
                self.new_line_point = None
                self.draw()
            else:
                self.new_line_point = new_point


class Scene:
    def __init__(self, points=[], lines=[]):
        self.points = points
        self.lines = lines


class PointsCollection:
    def __init__(self, points=[], **kwargs):
        self.points = points
        self.kwargs = kwargs

    def add_points(self, points):
        # print("PointsCollection; add_points")
        self.points = self.points + points


class LinesCollection:
    def __init__(self, lines=[], **kwargs):
        self.lines = lines
        self.kwargs = kwargs

    def add(self, line):
        # print("LinesCollection; add; line:")
        # print(line)
        self.lines.append(line)

    def get_collection(self):
        # print("LinesCollection; get_collection")
        return mcoll.LineCollection(self.lines, **self.kwargs)


class Plot:
    def __init__(self, scenes=[], json=None):
        if json is None:
            self.scenes = scenes
        else:
            self.scenes = [Scene([PointsCollection(pointsCol) for pointsCol in scene["points"]],
                                 [LinesCollection(linesCol) for linesCol in scene["lines"]])
                           for scene in js.loads(json)]

    def add_scene(self, scene):
        # print("Plot; add_scene")
        self.scenes.append(scene)

    def add_scenes(self, scenes):
        # print("Plot; add_sceneS")
        self.scenes = self.scenes + scenes

    def toJson(self):
        return js.dumps([{"points": [np.array(pointCol.points).tolist() for pointCol in scene.points],
                          "lines": [linesCol.lines for linesCol in scene.lines]}
                         for scene in self.scenes])

    def __configure_buttons(self, callback):
        plt.subplots_adjust(bottom=0.2)
        ax_add_random = plt.axes([0.02, 0.05, 0.15, 0.075])
        ax_add_line = plt.axes([0.18, 0.05, 0.15, 0.075])
        ax_add_point = plt.axes([0.34, 0.05, 0.15, 0.075])
        ax_prev = plt.axes([0.50, 0.05, 0.15, 0.075])
        ax_next = plt.axes([0.66, 0.05, 0.15, 0.075])
        ax_save_data = plt.axes([0.82, 0.05, 0.15, 0.075])
        b_next = Button(ax_next, 'Następny')
        b_next.on_clicked(callback.next)
        b_prev = Button(ax_prev, 'Poprzedni')
        b_prev.on_clicked(callback.prev)
        b_add_point = Button(ax_add_point, 'Dodaj punkt')
        b_add_point.on_clicked(callback.add_point)
        b_add_line = Button(ax_add_line, 'Dodaj linię')
        b_add_line.on_clicked(callback.add_line)
        b_add_random = Button(ax_add_random, 'Losowe linie')
        b_add_random.on_clicked(callback.add_random_lines)
        b_save_data = Button(ax_save_data, 'Zapisz dane')
        b_save_data.on_clicked(callback.save_data)
        return [b_prev, b_next, b_add_point, b_add_line, b_add_random, b_save_data]

    def draw(self):
        plt.close()
        fig = plt.figure()
        fig.set_tight_layout(False)
        callback = _Button_callback(self.scenes)
        self.widgets = self.__configure_buttons(callback)
        ax = plt.axes()
        callback.set_axes(ax)
        fig.canvas.mpl_connect('button_press_event', callback.on_click)
        plt.show()
        callback.draw()

# proste rysowanie

# scenes = [Scene([PointsCollection([(1, 2), (3, 1.5), (2, -1)]),
#                  PointsCollection([(5, -2), (2, 2), (-2, -1)], color='green', marker="^")],
#                 [LinesCollection([[(1, 2), (2, 3)], [(0, 1), (1, 0)]])]),
#           Scene([PointsCollection([(1, 2), (3, 1.5), (2, -1)], color='red'),
#                  PointsCollection([(5, -2), (2, 2), (-2, 1)], color='black')],
#                 [LinesCollection([[(-1, 2), (-2, 3)], [(0, -1), (-1, 0)]])])]
#
# plot = Plot(scenes)
# plot.add_scene(Scene([PointsCollection([(2, 1)])], [LinesCollection([[(1, 2), (2, 3)]])]))
# plot.draw()

# Zapis i odczyt z pliku¶

# Klasa Plot posiada metodę toJson(), która zwraca string zawierający listę scen w formacie JSON.
# Taki string można normalnie zapisać do pliku stosując normalne sposoby dostępne w Pythonie.
# Wczytanie listy scen z pliku dokonuje się poprzez podanie parametru json w kostruktorze Plot.

# scenes = [Scene([PointsCollection([(-2, -1)], color='green', marker="^")],
#                 [LinesCollection([[(1, 2), (2, 3)], [(0, 1), (1, 0)]])]),
#           Scene([PointsCollection([(1, 2), (3, 1.5)], color='red'),
#                  PointsCollection([(5, -2)], color='black')])]
# plot = Plot(scenes)
#
# with open('somefile.json', 'w') as file:
#     file.write(plot.toJson())
#
# # somefile.txt: [{"points": [[[-2, -1]]], "lines": [[[[1, 2], [2, 3]], [[0, 1], [1, 0]]]]}, {"points": [[[1.0, 2.0], [3.0, 1.5]], [[5, -2]]], "lines": []}]
#
# with open('somefile.json', 'r') as file:
#     json = file.read()
#
# plot2 = Plot(json=json)
# plot2.draw()
