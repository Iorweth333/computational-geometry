import math
from numpy.linalg import solve
import numpy as np
from lab1_determinants import determinants as dets
from lab3_intersections.interactive_visualisation import LinesCollection
from lab3_intersections.interactive_visualisation import PointsCollection
from lab3_intersections.interactive_visualisation import Scene


# http://wazniak.mimuw.edu.pl/index.php?title=Zaawansowane_algorytmy_i_struktury_danych/Wyk%C5%82ad_11#przecinanie_sie_odcinkow
# http://wazniak.mimuw.edu.pl/index.php?title=Zaawansowane_algorytmy_i_struktury_danych/Wyk%C5%82ad_12


def is_on_line_segment(s1p1, s1p2, p):
    """Assuming the points are collinear"""
    s1p1_x = s1p1[0]
    s1p2_x = s1p2[0]
    p_x = p[0]
    s1p1_y = s1p1[1]
    s1p2_y = s1p2[1]
    p_y = p[1]
    if (min(s1p1_x, s1p2_x) < p_x < max(s1p1_x, s1p2_x)) and \
            (min(s1p1_y, s1p2_y) < p_y < max(s1p1_y, s1p2_y)):
        return True
    return False


def do_segments_intersect(segment1, segment2):
    s1p1 = segment1[0]
    s1p2 = segment1[1]
    s2p1 = segment2[0]
    s2p2 = segment2[1]
    d1 = dets.det1(s2p1, s2p2, s1p1)
    d2 = dets.det1(s2p1, s2p2, s1p2)
    d3 = dets.det1(s1p1, s1p2, s2p1)
    d4 = dets.det1(s1p1, s1p2, s2p2)
    if d1 * d2 < 0 and d3 * d4 < 0:
        return True
    if (math.fabs(d1) < dets.default_precision and is_on_line_segment(s2p1, s2p2, s1p1)) or \
            (math.fabs(d2) < dets.default_precision and is_on_line_segment(s2p1, s2p2, s1p2)) or \
            (math.fabs(d3) < dets.default_precision and is_on_line_segment(s1p1, s1p2, s2p1)) or \
            (math.fabs(d4) < dets.default_precision and is_on_line_segment(s1p1, s1p2, s2p2)):
        return True
    return False


def calculate_intersection_point(segment1, segment2):
    s1p1_x = segment1[0][0]  # coords od segments' points
    s1p1_y = segment1[0][1]
    s1p2_x = segment1[1][0]
    s1p2_y = segment1[1][1]
    s2p1_x = segment2[0][0]
    s2p1_y = segment2[0][1]
    s2p2_x = segment2[1][0]
    s2p2_y = segment2[1][1]
    # y = ax + b
    matrix_of_unknown = np.array([[s1p1_x, 1], [s1p2_x, 1]])
    vector_of_results = np.array([s1p1_y, s1p2_y])
    solved = solve(matrix_of_unknown, vector_of_results)
    line1_eq_a = solved[0]  # a and b parameters of equation of line including segment1
    line1_eq_b = solved[1]
    matrix_of_unknown = np.array([[s2p1_x, 1], [s2p2_x, 1]])
    vector_of_results = np.array([s2p1_y, s2p2_y])
    solved = solve(matrix_of_unknown, vector_of_results)
    line2_eq_a = solved[0]
    line2_eq_b = solved[1]
    # y - ax = b    # REMEMBER a is negated
    matrix_of_unknown = np.array([[1, -line1_eq_a], [1, -line2_eq_a]])
    vector_of_results = np.array([line1_eq_b, line2_eq_b])
    solved = solve(matrix_of_unknown, vector_of_results)
    x = solved[1]
    y = solved[0]
    intersection_point = (x, y)
    return intersection_point


def find_intersections(segments: list):
    segment_copy = segments.copy()
    pois = []  # points of interest
    for i, segment in enumerate(segment_copy):
        pois.append((segment[0][0], segment[0][1], i))
        pois.append((segment[1][0], segment[1][1], i))
    pois.sort(key=lambda tup: tup[0])  # sort by x; left to right

    sweeper_state = []  # indices of segments that the sweeper intersects
    intersections = []

    for poi in pois:
        poi_x = poi[0]
        poi_y = poi[1]
        poi_line_index = poi[2]
        if poi_line_index in sweeper_state:
            sweeper_state.remove(poi_line_index)
        else:
            sweeper_state.append(poi_line_index)
            for i in range(len(sweeper_state)):
                for j in range(i + 1, len(sweeper_state)):
                    segment_i = segment_copy[sweeper_state[i]]
                    segment_j = segment_copy[sweeper_state[j]]
                    if do_segments_intersect(segment_i, segment_j):
                        intersection_point = calculate_intersection_point(segment_i, segment_j)
                        intersection = (segment_i, segment_j, intersection_point)
                        if intersection not in intersections:
                            intersections.append(intersection)
                            # może faktycznie wariant ze sprawdzaniem dwóch sąsiednich na miotle były mądrzejszy
                            # ciekawe czy w pytongu jest bst z zadanym kluczem będącym lambdą
    return intersections


def create_scene(poi_x, intersections: list, all_lines: list, lines_indices: list, top_sweeper_y, bot_sweeper_y):
    """poi = Point Of Interst"""
    sweeper__itself = LinesCollection(lines=[[(poi_x, top_sweeper_y), (poi_x, bot_sweeper_y)]], color='red')
    intersection_points = []
    for intersection in intersections:
        intersection_points.append((intersection[2]))

    sweeper_lines = []
    for index in lines_indices:
        sweeper_lines.append(all_lines[index])

    intersection_points_collection = PointsCollection(intersection_points)
    sweeper_lines_collection = LinesCollection(sweeper_lines, color='yellow')
    all_lines_copy = []
    for line in all_lines:
        if line not in sweeper_lines:
            all_lines_copy.append(line)
    all_lines_collection = LinesCollection(all_lines_copy)
    return Scene([intersection_points_collection], [sweeper_lines_collection, all_lines_collection, sweeper__itself])


def find_intersections_with_scenes(segments: list):
    scenes = []
    segments_copy = segments.copy()
    pois = []  # points of interest
    for i, segment in enumerate(segments_copy):
        pois.append((segment[0][0], segment[0][1], i))
        pois.append((segment[1][0], segment[1][1], i))
    pois.sort(key=lambda tup: tup[0])  # sort by x; left to right

    max_y = max(pois, key=lambda tup: tup[1])[1]  # for sweeper line
    min_y = min(pois, key=lambda tup: tup[1])[1]

    sweeper_state = []  # indices of segments that the sweeper intersects
    intersections = []

    for poi in pois:
        poi_x = poi[0]
        poi_y = poi[1]
        poi_line_index = poi[2]
        if poi_line_index in sweeper_state:
            sweeper_state.remove(poi_line_index)
        else:
            sweeper_state.append(poi_line_index)
            for i in range(len(sweeper_state)):
                for j in range(i + 1, len(sweeper_state)):
                    segment_i = segments_copy[sweeper_state[i]]
                    segment_j = segments_copy[sweeper_state[j]]
                    if do_segments_intersect(segment_i, segment_j):
                        intersection_point = calculate_intersection_point(segment_i, segment_j)
                        intersection = (segment_i, segment_j, intersection_point)
                        if intersection not in intersections:
                            intersections.append(intersection)
                            # może faktycznie wariant ze sprawdzaniem dwóch sąsiednich na miotle były mądrzejszy
                            # ciekawe czy w pytongu jest bst z zadanym kluczem będącym lambdą
        scenes.append(create_scene(poi_x, intersections, segments_copy, sweeper_state, max_y, min_y))
    return scenes
