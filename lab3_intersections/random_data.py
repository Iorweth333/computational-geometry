import random
import lab3_intersections.interactive_visualisation


def random_lines(number_of_lines=1, min_x=-100, min_y=-100, max_x=100, max_y=100):
    created_lines_number = 0
    lines = []
    while created_lines_number < number_of_lines:
        x1 = random.uniform(min_x, max_x)
        y1 = random.uniform(min_y, max_y)
        x2 = random.uniform(min_x, max_x)
        y2 = random.uniform(min_y, max_y)
        if y1 == y2:    #cant't be horizontal: assumption
            continue
        for line in lines:
            if (x1 == line[0][0] and y1 == line[0][1]) or (x2 == line[1][0] and y2 == line[1][1]):  #can't touch with ends: assumption
                continue
        lines.append([(x1, y1), (x2, y2)])
        created_lines_number += 1
    return lab3_intersections.interactive_visualisation.LinesCollection(lines)
