from lab3_intersections.sweeper_algorithm import do_segments_intersect
from lab3_intersections.interactive_visualisation import Plot
from lab3_intersections.interactive_visualisation import LinesCollection
from lab3_intersections.interactive_visualisation import PointsCollection
from lab3_intersections.interactive_visualisation import Scene
from lab3_intersections.sweeper_algorithm import find_intersections
from lab3_intersections.sweeper_algorithm import find_intersections_with_scenes
import json as js

# line1 = [(0, 1), (1, 0)]
# line2 = [(-2, 0), (0, 2)]
# if do_segments_intersect(line1[0], line1[1], line2[0], line2[1]):
#     print("they intersect")
# else:
#     print("they don't intersect")
#
# line1 = [(0, 1), (1, 0)]
# line2 = [(0, 0), (1, 1)]
# if do_segments_intersect(line1, line2):
#     print("they intersect")
#     intersections = find_intersections([line1, line2])
#     print(intersections[0][2])
# else:
#     print("they don't intersect")




# with open('plot_data.json') as json_file:
#     json_data = js.load(json_file)
#     segments = json_data[0]["lines"]
#     for index, segment in enumerate(segments):
#         segments[index] = segment[0]
#     intersections = find_intersections(segments)
#     print("intersections: ")
#     intersections_just_points = []
#     for intersection in intersections:
#         print(intersection)
#         intersections_just_points.append(intersection[2])
#     print("intersections number: " + str(len(intersections)))
#     linesCollectionList = [LinesCollection(lines=segments)]
#     pointsCollectionList = [PointsCollection(points=intersections_just_points)]
#     scenes = [Scene(lines=linesCollectionList, points=pointsCollectionList)]
#     plot = Plot(scenes)
#     print("draw:")
#     plot.draw()

with open('plot_data.json') as json_file:
    json_data = js.load(json_file)
    segments = json_data[0]["lines"]
    for index, segment in enumerate(segments):
        segments[index] = segment[0]
    scenes = find_intersections_with_scenes(segments)
    plot = Plot(scenes)
    print("draw:")
    plot.draw()



# scenes = [Scene([PointsCollection([(-2, -1)], color='green', marker="^")],
#                 [LinesCollection([[(1, 2), (2, 3)], [(0, 1), (1, 0)]])]),
#           Scene([PointsCollection([(1, 2), (3, 1.5)], color='red'),
#                  PointsCollection([(5, -2)], color='black')])]
# plot = Plot(scenes)
